function [t,tt,y4,y_ana,u_m,u_m2,Error_steady_abs,Error_steady_rel,Error_max_rel,Error_mean_rel, Error_max, SettleTime, Error_mean, a, b, c]=Fun_ClosedLoopControl_hayami_test(q,n,Bw,m,Sb,Y0,h_real_initial,L,kh,q_downstream,factor_sim_time,eMAVE,duMAVE)

%The int part is not corrected at all
%second order int 0 is not done
% With this model all the three possiblities should be tested 
%1. First order without delay



% clear
% n=0.02;                     %Manning factor [-]
% Bw=3;                       %Bottom width [m]
% 
% close all
% 
% L=7000;
% q=1;
% m=0;
% Sb=0.0003;
% Y0=1.2;
% h_real_initial=Y0;
% q_downstream=1.5;
% factor_sim_time=30;
% eMAVE=0.3;
% %eMAVE=50000;
% 
% duMAVE=0.02;
% kh=0;
% UseTravelTime=0;
% Mr_db=0;
% Om_r=0;
[ae tau]=IdFun(q,n,Bw,m,Sb,Y0,L,kh);


%% Simulation variables
sampling_time=900;          %sampling time [s]
dt=sampling_time/10;        %discretization time of the SV model [s] -- Should be an integer
simulation_time=tau*factor_sim_time;     %simulation time [s]

%Uncomment this if you would like to see the profile
% [ WaterProfile, distance] = SteadyState(L, m,Bw,n,Sb,q,Y0,100, 0);
% plot(WaterProfile)
%% Create a system by hand

%Internal Hayami model
[aa,bb,cc,dd,ee,r,cl,yn,ccnoni,ddnoni,eenoni,G,K1,tau,ae]=FunHayami(q,n,Bw,m,Sb,Y0,L,kh,dt);

int=floor(tau/sampling_time);       %integrator [t_d/dt]
t_f=tau-sampling_time*int;      

if cl<9/4       %first order with(out) delay
    n2=(K1*exp(-1*(sampling_time-t_f)/K1)+sampling_time-t_f-K1)/ae;
    n1=(-2*K1*exp(-1*(sampling_time-t_f)/K1)+t_f+(-sampling_time+t_f+K1)*exp(-sampling_time/K1)+K1)/ae;
    n0=(K1*exp(-1*(sampling_time-t_f)/K1)+(-t_f-K1)*exp(-sampling_time/K1))/ae;
    d1=(-1-exp(-sampling_time/K1));
    d0=exp(-sampling_time/K1);

else            %second order with delay
    K2=K1(2);
    K1=K1(1);
    a1=exp(-sampling_time/K1);
    a2=exp(-sampling_time/K2);
    a3=(K1^2)/(K1-K2)*exp(-1*(sampling_time-t_f)/K1);
    a4=(K2^2)/(K1-K2)*exp(-1*(sampling_time-t_f)/K2);

    n3=(a3-a4+(sampling_time-t_f)-(K1+K2))/ae;
    n2=(-2*a3+2*a4-(sampling_time-t_f)*(a1+a2)+t_f-(K1+K2)*(-1*(a1+a2+1))-a2*a3+a1*a4)/ae;
    n1=(a3*(1+2*a2)-a4*(1+2*a1)+(sampling_time-t_f)*a1*a2-t_f*(a1+a2)-(K1+K2)*(a1+a2+a1*a2))/ae;
    n0=(-a2*a3+a1*a4+t_f*a1*a2+(K1+K2)*a1*a2)/ae;
    d2=(-a1-a2-1);
    d1=(a1+a2+a1*a2);
    d0=(-a1*a2);
    
    n0=n1;
    n1=n2;
    n2=n3;
    n3=0;
end
n0=real(n0);

%Construction of matrices
if cl<9/4       %first order with(out) delay
    system_size=5+int;                 %system size depends on delay
    a=zeros(system_size,system_size);
    if int==0
        a(1,1)=-d1;
        a(1,2)=-d0;
        a(1,3)=n2;
        a(1,4)=n1;
        a(1,5)=-sampling_time/(ae)*(1+d1);
        a(2,1)=1;
        a(4,3)=1;
        a(3,3)=1;
     else
        a(1,1)=-d1;
        a(1,2)=-d0;
        a(1,end-3)=n2;
        a(1,end-2)=n1;
        a(1,end-1)=n0;
        a(1,end)=-sampling_time/(ae)*(1+d1);
        a(2,1)=1;

        for k=1:1+int
            a(k+3,k+2)=1;
        end
        a(3,3)=1;
    end

    b=zeros(system_size,2);
    if int==0
        b(1,1)=0;
        b(1,2)=-sampling_time/ae;
        b(3,1)=1;
        b(5,2)=1;
    else
        b(1,2)=-sampling_time/ae;
        b(3,1)=1;
        b(end,2)=1;
    end
    
    c=zeros(1,system_size);
    c(1)=1;
    
else            %second order with delay
    system_size=8+int;                 %system size is dependend on delay
    a=zeros(system_size,system_size);
    if int==0
        a(1,1)=-d2;
        a(1,2)=-d1;
        a(1,3)=-d0;

        a(1,end-4)=n1;
        a(1,end-3)=n0;

        a(1,end-2)=-sampling_time/(ae)*(1+d2);
        a(1,end-1)=-sampling_time/(ae)*(d1+1+d2);

        a(2,1)=1;
        a(3,2)=1;

        a(end-1,end-2)=1;
        a(end,end-1)=1;
       
        for k=1:int+1
            a(k+4,k+3)=1;
        end
    else
        a(1,1)=-d2;
        a(1,2)=-d1;
        a(1,3)=-d0;

        a(1,end-5)=n2;
        a(1,end-4)=n1;
        a(1,end-3)=n0;

        %a(1,end-2)=-sampling_time/(ae)*(1+d2);
        %a(1,end-1)=-sampling_time/(ae)*(d1+1+d2);
       
        a(1,end-1)=-sampling_time/(ae)*(1+d2);
        a(1,end)=-sampling_time/(ae)*(d1+1+d2);

        a(2,1)=1;
        a(3,2)=1;
        
        a(end-1,end-2)=1;
        a(end,end-1)=1;
       
        for k=1:int+1
            a(k+4,k+3)=1;
        end
        a(4,4)=1;    
    end
    b=zeros(system_size,2);
    if int==0
        b(1,1)=n2;
        b(1,2)=-sampling_time/ae;
        b(4,1)=1;
        b(end-1,2)=1;
    else  %This is corercted, the above one not, I guess the above never happens: second order without delay.
        b(1,2)=-sampling_time/ae;
        b(4,1)=1;
        b(end-1,2)=1;
    end
    
    c=zeros(1,system_size);
    c(1)=1;
end

% sys=ss(a,b,c,0,900);
% %[y,t,x,ysd] =impulse(sys);
% t=0:900:900*1000;
% u=zeros(length(t),2);
% u(1:1,1)=1;
% %u(1:end,1)=1;
% 
% [y,t,x]=lsim(sys,u,t);
% plot(t,y)
%% Simulation
la=30;      %Prediction horizon

simulation_time_vector= 0:sampling_time:simulation_time;
t=0:sampling_time/3600:simulation_time/3600;                        %for adding time-scale to the plots
tt=0:sampling_time/3600:(simulation_time-1*sampling_time)/3600;     %for adding time-scale to the plots

%Input discharges
u=zeros(2,length(simulation_time_vector)+la);
u(1,1)=0;
u(2,14)=q_downstream-q;
u(2,15)=q_downstream-q;
u(2,16)=q_downstream-q;

x0=zeros(system_size,1);
x0(1,1)=h_real_initial-Y0;

%% Creating the big matrices
[A,B,C,Bd,Q,R]= MatrixContructionFunctionNew(a,b(:,1),c,b(:,2),la,eMAVE,duMAVE);

save testvar A B Bd a b Q R
%% creating uncontrolled behaviour
x=x0;
x(1,1)=h_real_initial-Y0;
y6_real(1)=h_real_initial;

for k=1+int:length(u)-1
    x=a*x+b*[u(1,k-int);u(2,k)];
    y=c*x;
    y6(k+1)=y;
    y6_real(k+1)=y6(k+1)+Y0;
    ttt(k+1)=k*sampling_time/3600;
end

% figure
% plot(ttt,y6);grid
% title('Water level (downstream) after step in downstream discharge')
% xlabel('time (h)')
% ylabel('water level (m)')

%% Now creating a control

x=x0;
y4(1)=h_real_initial;
D_time=zeros(la,1);
u_int1=u;
u_int2=u;
u_int3=u;
u_int4=u;
xres1=0;
for k=1:floor(simulation_time/sampling_time)
   
    
%    xtest=a*x+b*[u(1,1);D_time(1)];           %updating discharge in x
    x=a*x+b*[xres1(1);D_time(1)];    %updating discharge in x

 %   x=xtest;
    x(1,1)=y4(k)-Y0;        %updating the water level difference every time step
    
    %Updating disturbance
    D_time(1:la) = u(2,k:k+la-1);
   
    H=2*(B'*Q*B+R);
    f=2*x'*(A'*Q*B)+2*D_time'*(Bd'*Q*B);        

     options =  optimoptions(@quadprog,'Display','off');
%    [xres,fval,exitflag]=quadprog(H,f,[],[],[],[],[],[],[],options );
   xres1 = - inv(H)*f';
    xres2=xres1*0;
    xres2(1)=u(1,1)+xres1(1);
    for kk=2:length(xres1)
        xres2(kk)=xres2(kk-1)+xres1(kk);
    end
       xres= xres2;
    u(1,1:la)=xres';
    
    %Creates Q inputs (long vectors) for the simulation
    qu_used=ones(floor(sampling_time/dt)+4,1)*(u(1,1)+q);       %real controlled input   
    qd_used=ones(floor(sampling_time/dt)+4,1)*(u(2,k)+q);       %real disturbance
    if k==1
        [Time,Hm2,Hm,HinitH]=FiniteVoumeModel11bck(L,m,Bw,n,Sb,sampling_time,dt,qu_used,qd_used,y4(k),300,0,0);
    else
        [Time,Hm2,Hm,HinitH]=FiniteVoumeModel11bck(L,m,Bw,n,Sb,sampling_time,dt,qu_used,qd_used,y4(k),300,1,Hm2(end,:));
    end
    
    %Saving variables
    y4(k+1)=Hm2(end,end);
    u_m(k)=u(2,k);
    u_m2(k)=u(1,1);
    
    Error(k+1)=abs(y4(k+1)-Y0);         %for analyses
end
y2_real(1)=h_real_initial;

%% analyses
y_ana=zeros(1,floor(simulation_time/sampling_time));
y_ana(1,:)=Y0; 
Error_steady_abs=abs(y4(end)-Y0);               %for analyses
Error_steady_rel=abs(y4(end)-Y0)/Y0;            %for analyses
Error_max_rel=max(Error)/Y0;                    %for analyses
Error_mean_rel=mean(Error)/Y0;                  %for analyses
Error_max=max(abs(Error));                    %for analyses
Error_mean=mean(abs(Error));                  %for analyses

Switch = 0;
SettleTime=inf;
%Threshold = 0.003;
Threshold = 0.015;
for k=length(Error):-1:1
    if abs(Error(k))> Threshold && k==length(Error)
        SettleTime = inf;
    else    
    if abs(Error(k)) > Threshold && Switch ==0
        SettleTime=k*sampling_time;
        Switch = 1;
    end
    end
end

% fprintf('Steady state error = %f m\n', Error_steady);
% fprintf('Maximum error      = %f m\n', Error_max);
% fprintf('Mean error         = %f m\n', Error_mean);
% fprintf('int                = %d \n', int);
% fprintf('cl                = %d \n', cl);
% 
%%
% figure
% subplot(2,1,1)
% plot(t,y4,'--*');grid
% hold on
% plot(tt,y_ana)
% title('Water level (downstream)')
% xlabel('time (h)')
% ylabel('water level (m)')
% 
% subplot(2,1,2)
% plot(tt,u_m, '-*')
% hold on
% plot(tt,u_m2, '-*r');grid
% % legend('Downstream (given) Q', 'Upstream (Controlled) Q','location','BestOutside')
% title('Discharges')
% xlabel('time (h)')
% ylabel('discharge (m3/s)')