function [t,tt,y4,y_ana,u_m,u_m2,Error_steady_abs,Error_steady_rel,Error_max_rel,Error_mean_rel, Error_max, SettleTime, Error_mean,a, b, c]=Fun_ClosedLoopControl_Muskingum_test(q,n,Bw,m,Sb,Y0,h_real_initial,L,kh,q_downstream,factor_sim_time,eMAVE,duMAVE)
% clear
% n=0.02;                     %Manning factor [-]
% Bw=3;                       %Bottom width [m]
% 
% close all
% 
% L=7000;
% q=1;
% m=0;
% Sb=0.0001;
% Y0=1.2;
% h_real_initial=Y0;
% q_downstream=0.5;
% factor_sim_time=30;
% eMAVE=0.01;
% duMAVE=0.02;
% kh=0;
%q,n,Bw,m,Sb,Y0,h_real_initial,L,kh,q_downstream,factor_sim_time,eMAVE,duMAV

[ae tau T]=IdFun(q,n,Bw,m,Sb,Y0,L,kh);

%% Simulation variables
sampling_time=900;          %sampling time [s]
dt=sampling_time/10;      %discretization time of the SV model [s] -- Should be an integer
simulation_time=tau*factor_sim_time;     %simulation time [s]
%simulation_time=5e4;
%Uncomment this if you would like to see the profile
% [ WaterProfile, distance] = SteadyState(L, m,Bw,n,Sb,q,Y0,100, 0);
% plot(WaterProfile)
%% Create a system by hand
         
% Calculation of K and X
g=9.81;             %[m/s^2]
a=Bw*Y0+m*Y0^2;     %flow area canal [m^2]
c=(g*a/Bw)^0.5;     %velocity of the wave [m/s]
v=q/a;              %flow velocity [m/s]
K=L/(v+c);          
X=0.5*(1-1/(L*(Y0*g)^0.5*Bw*Sb/q));           

if X>0.5
    X=0.5;
end

if X<0
    X=0;
end

% group 1-6
g1=(2-dt/(K*(1-X)));
g2=(dt/(K*(1-X))-1);
g3=(-dt*X/((1-X)*ae));
g4=(dt^2/(K*(1-X)*ae)+dt*X/((1-X)*ae));
g5=(-dt^2/(K*(1-X)*ae)+dt/ae);
g6=(dt/(ae));

a1=exp(-sampling_time/(K*(1-X)));
n1=(sampling_time-K+K*a1)/ae;
n0=(-sampling_time*a1-K*a1+K)/ae;
d1=(-1-a1);
d0=a1;

% % creation of matrix a
% system_size=7;        % system size is always 7 
% a=zeros(system_size,system_size);
% a(1,1)=1-d1;
% a(1,2)=d1-d0;
% a(1,3)=d0;
% a(1,4)=n0-n1;
% a(1,5)=-n0;
% a(1,6)=-d1*sampling_time/ae;
% a(1,7)=-d0*sampling_time/ae;
% a(2,1)=1;
% a(3,2)=1;
% a(5,4)=1;
% a(7,6)=1;
% 
% % creation of matrix b
% b=zeros(system_size,2);
% b(1,1)=n1;
% b(1,2)=-sampling_time/ae;
% b(4,1)=1;
% b(6,2)=1;

% creation of matrix a
system_size=4;        % system size is always 7 
a=zeros(system_size,system_size);
a(1,1)=-d1;
a(1,2)=-d0;

a(1,3)=n0;
a(1,4)=-(1+d1)*sampling_time/ae;
a(2,1)=1;


% creation of matrix b
b=zeros(system_size,2);
b(1,1)=n1;
b(1,2)=-sampling_time/ae;
b(3,1)=1;
b(4,2)=1;

aold=a;
bold=b;







%For incremental formulation
system_size=5;
a=zeros(system_size,system_size);
a(1,1)=-d1;
a(1,2)=-d0;
a(1,3)=n1;
a(1,4)=n0;
a(1,5)=-(1+d1)*sampling_time/ae;
a(2,1)=1;

a(3,3)=1;
a(4,3)=1;

b=zeros(system_size,2);
b(1,2)=-sampling_time/ae;
b(3,1)=1;
b(5,2)=1;
b(3,1)=1;


%For incremental formulation
system_size=5;
a=zeros(system_size,system_size);
a(1,1)=-d1;
a(1,2)=-d0;
a(1,3)=n1+n0;
a(1,4)=0;
a(1,5)=-(1+d1)*sampling_time/ae;
a(2,1)=1;

a(3,3)=1;
a(4,3)=1;

b=zeros(system_size,2);
b(1,2)=-sampling_time/ae;
b(1,1)=n1;
b(3,1)=1;
b(5,2)=1;
b(3,1)=1;

% creation of matrix c
c=zeros(1,system_size);
c(1,1)=1;

% sys=ss(a,b,c,0,900);
% %[y,t,x,ysd] =impulse(sys);
% t=0:900:900*1000;
% u=zeros(length(t),2);
% %u(1:1,1)=1;
% u(1:end,2)=1;
% 
% [y,t,x]=lsim(sys,u,t);

%% Simulation
la=30;        %prediction horizon

simulation_time_vector=0:sampling_time:simulation_time;
t=0:sampling_time/3600:simulation_time/3600;                        %for adding time-scale to the plots
tt=0:sampling_time/3600:(simulation_time-1*sampling_time)/3600;     %for adding time-scale to the plots


%input discharges
u=zeros(2,length(simulation_time_vector)+la);
u(1,1)=0;
u(2,14)=q_downstream-q;
u(2,15)=q_downstream-q;
u(2,16)=q_downstream-q;

x0=zeros(system_size,1);
x0(1,1)=h_real_initial-Y0;

%% Creating the big matrices
[A,B,C,Bd,Q,R]= MatrixContructionFunctionNew(a,b(:,1),c,b(:,2),la,eMAVE,duMAVE);
% [nn, mm]=size(b);
% mm=1;
% [out_dim, ~]=size(c);
% 
% if duMAVE == 0
%     r = zeros(mm,mm);
% else
%     r=eye(mm) * 1/(duMAVE^2);
% end
% 
% if eMAVE == 0
%     q_out = zeros(out_dim, out_dim);
% else
%     q_out = eye(out_dim) * 1 / (eMAVE^2) ;
% end
% qmatr = c'*q_out*c;
% qmatr(3,3)= 1/(duMAVE^2);
% R=[r];
% for k=1:la-1
%   z=zeros(mm*k,mm);
%   R=[R z; z' r];
% end
% 
% Q=[qmatr];
% for k=1:la-1
%   z=zeros((nn)*k,(nn));
%   Q=[Q z; z' qmatr];
% end



%% creating uncontrolled behaviour
x = x0;
x(1,1)=h_real_initial-Y0;
y6_real(1)=h_real_initial;

for k=1:length(u)-1
    x=a*x+b*u(:,k);
    y=c*x;
    y6(k+1)=y;
    y6_real(k+1)=y6(k+1)+Y0;
    ttt(k+1)=k*sampling_time/3600;
end

% figure
% plot(ttt,y6_real);grid
% title('Water level (downstream) after step in downstream discharge')
% xlabel('time (h)')
% ylabel('water level (m)')

%% Now creating a control

x=x0;
y4(1)=h_real_initial;
D_time=zeros(la,1);
xres1=0;

xtest_old=zeros(4,1);
xtest_m = zeros(5,2);
xtest_old_m=zeros(4,2);
for k=1:floor(simulation_time/sampling_time)
%for k=1:40
    xtest_old=aold*xtest_old+bold*[u(1,1);D_time(1)];    %updating discharge in x
     x=a*x+b*[xres1(1);D_time(1)];    %updating discharge in x

    %x=xtest;
    x(1,1)=y4(k)-Y0  ;                 %updating x difference every time step
    xtest_old(1,1)=y4(k)-Y0;
    %Updating disturbance
    D_time(1:la)=u(2,k:k+la-1); 

    H=2*(B'*Q*B+R);
    f=2*x'*(A'*Q*B)+2*D_time'*(Bd'*Q*B);        

%     options =  optimoptions(@quadprog,'Display','off');
     
 %   [xres1,fval,exitflag]=quadprog(H,f,[],[],[],[],[],[],[],options );
    xres1 = - inv(H)*f';
    xres2=xres1*0;
    xres2(1)=u(1,1)+xres1(1);
    for kk=2:length(xres1)
        xres2(kk)=xres2(kk-1)+xres1(kk);
    end
       xres= xres2;

    u(1,1:la)=xres';
    
    %Creates Q inputs (long vectors) for the simulation
    qu_used=ones(floor(sampling_time/dt)+4,1)*(u(1,1)+q);       %real controlled input   
    qd_used=ones(floor(sampling_time/dt)+4,1)*(u(2,k)+q);       %real disturbance
    if k==1
        [Time,Hm2,Hm,HinitH]=FiniteVoumeModel11bck(L,m,Bw,n,Sb,sampling_time,dt,qu_used,qd_used,y4(k),300,0,0);
    else
        [Time,Hm2,Hm,HinitH]=FiniteVoumeModel11bck(L,m,Bw,n,Sb,sampling_time,dt,qu_used,qd_used,y4(k),300,1,Hm2(end,:));
    end
    
    %Saving variables
    y4(k+1)=Hm2(end,end);
    u_m(k)=u(2,k);
    u_m2(k)=u(1,1);
    
    Error(k+1)=abs(y4(k+1)-Y0);         %for analyses
    xtest_old_m(:,k)=xtest_old;
    xtest_m(:,k)=x;
end

%% analyses
y_ana=zeros(1,floor(simulation_time/sampling_time));
y_ana(1,:)=Y0; 
Error_steady_abs=abs(y4(end)-Y0);               %for analyses
Error_steady_rel=abs(y4(end)-Y0)/Y0;            %for analyses
Error_max_rel=max(Error)/Y0;                    %for analyses
Error_mean_rel=mean(Error)/Y0;                  %for analyses
Error_max=max(abs(Error));                    %for analyses
Error_mean=mean(abs(Error));                  %for analyses

Switch = 0;
SettleTime=inf;
%Threshold = 0.003;
Threshold = 0.015;
for k=length(Error):-1:1
    if abs(Error(k))> Threshold && k==length(Error)
        SettleTime = inf;
    else    
    if abs(Error(k)) > Threshold && Switch ==0
        SettleTime=k*sampling_time;
        Switch = 1;
    end
    end
end
 
% fprintf('Absolute steady state error = %f m\n', Error_steady_abs);
% fprintf('Relative steady state error = %f m\n', Error_steady_rel);
% fprintf('Relative maximum error      = %f m\n', Error_max_rel);
% fprintf('Relative mean error         = %f m\n', Error_mean_rel);
% 
% %%
% % figure
% subplot(2,1,1)
% plot(t,y4,'--*');grid
% hold on
% plot(tt,y_ana)
% title('Water level (downstream)')
% xlabel('time (h)')
% ylabel('water level (m)')
% 
% subplot(2,1,2)
% plot(u_m, '-*')
% hold on
% plot(u_m2, '-*r');grid
% % legend('Downstream (given) Q', 'Upstream (Controlled) Q','location','BestOutside')
% title('Discharges')
% xlabel('time (h)')
% ylabel('discharge (m3/s)')