function [t,tt,y4,y_ana,u_m,u_m2,Error_steady_abs,Error_steady_rel,Error_max_rel,Error_mean_rel, Error_max, SettleTime, Error_mean, a, b, c]=Fun_ClosedLoopControl_ID_test(q,n,Bw,m,Sb,Y0,h_real_initial,L,kh,q_downstream,factor_sim_time,eMAVE,duMAVE)
% clear
% n=0.02;                     %Manning factor [-]
% Bw=3;                       %Bottom width [m]
% 
% close all
% 
% L=7000;
% q=2;
% m=2;
% Sb=0.00015;
% Y0=1;
% h_real_initial=Y0;
% q_downstream=2.5;
% factor_sim_time=50;
% eMAVE=0.2;
% duMAVE=0.02;
% kh=0;

[ae, tau T]=IdFun(q,n,Bw,m,Sb,Y0,L,kh);
%tau=tau+900;
%% Simulation variables
sampling_time=900;          %sampling time [s]
dt=sampling_time/10;        %discretization time of the SV model [s] -- Should be an integer
simulation_time=tau*factor_sim_time;     %simulation time [s]
%% Create a system by hand
system_size=floor(tau/sampling_time)+2;
a=zeros(system_size,system_size);
a(1,1)=1;
a(1,end)=sampling_time/ae;
for k=3:system_size
    a(k,k-1)=1;
end
a(2,2)=1;
b=zeros(system_size,2);
b(2,1)=1;
b(1,2)=-sampling_time/ae;


%---------------

% system_size=floor(tau/sampling_time)+1;
% a=zeros(system_size,system_size);
% a(1,1)=1;
% a(1,end)=sampling_time/ae;
% for k=3:system_size
%     a(k,k-1)=1;
% end
% a(2,2)=1;
% b=zeros(system_size,2);
% b(2,1)=1;
% b(1,2)=-sampling_time/ae;


%---------

% system_size=floor(tau/sampling_time)+2;
% a=zeros(system_size,system_size);
% a(1,1)=1;
%a(2,2)=1;

% a(1,end)=sampling_time/ae;
% for k=3:system_size
%     a(k,k-1)=1;
% end
% 
% b=zeros(system_size,2);
% b(2,1)=1;
% b(1,2)=-sampling_time/ae;



c=zeros(1,system_size);
c(1)=1;
save test a b c tau
% sys=ss(a,b,c,0,900);
% %[y,t,x,ysd] =impulse(sys);
% t=0:900:900*1000;
% u=zeros(length(t),2);
% u(1:1,1)=1;
% %u(1:end,1)=1;
% 
% [y,t,x]=lsim(sys,u,t);
% plot(t,y)
%% Simulation
la=30;        %prediction horizon

simulation_time_vector=0:sampling_time:simulation_time;            
t=0:sampling_time/3600:simulation_time/3600;                       %for adding time-scale to the plots
tt=0:sampling_time/3600:(simulation_time-1*sampling_time)/3600;    %for adding time-scale to the plots

%discharge input
u=zeros(2,length(simulation_time_vector)+la);
u(1,1)=0;
u(2,14)=q_downstream-q;
u(2,15)=q_downstream-q;
u(2,16)=q_downstream-q;

x0=zeros(system_size,1);
x0(1,1)=h_real_initial-Y0;

%% Creating the big matrices
[A,B,C,Bd,Q,R]=MatrixContructionFunctionNew(a,b(:,1),c,b(:,2),la,eMAVE,duMAVE);
% [nn, mm]=size(b);
% mm=1;
% [out_dim, ~]=size(c);
% 
% if duMAVE == 0
%     r = zeros(mm,mm);
% else
%     r=eye(mm) * 1/(duMAVE^2);
% end
% 
% if eMAVE == 0
%     q_out = zeros(out_dim, out_dim);
% else
%     q_out = eye(out_dim) * 1 / (eMAVE^2) ;
% end
% qmatr = c'*q_out*c;
% qmatr(2,2)= 1/(duMAVE^2);
% R=[r];
% for k=1:la-1
%   z=zeros(mm*k,mm);
%   R=[R z; z' r];
% end
% 
% Q=[qmatr];
% for k=1:la-1
%   z=zeros((nn)*k,(nn));
%   Q=[Q z; z' qmatr];
% end

%% creating uncontrolled behaviour
x = x0;
x(1,1)=h_real_initial-Y0;
y6_real=h_real_initial;

for k=1:length(u)-1
    x=a*x+b*u(:,k);
    y=c*x;
    y6(k+1)=y;
    y6_real(k+1)=y6(k+1)+Y0;
    ttt(k+1)=k*sampling_time/3600;
end

% figure
% plot(ttt,y6_real);grid
% title('Water level (downstream) after step in downstream discharge')
% xlabel('time (h)')
% ylabel('water level (m)')

%% Now creating a control

x=x0;
y4(1)=h_real_initial;
D_time=zeros(la,1);
xres1=0;
for k=1:floor(simulation_time/sampling_time)
%for k=1:floor(simulation_time/sampling_time)

%    xtest=a*x+b*[u(1,1);D_time(1)];     %updating discharge in x
     xtest=a*x+b*[xres1(1);D_time(1)];     %updating discharge in x

    x=xtest;
    x(1,1)=y4(k)-Y0;                    %updating the water level difference every time step
    
    %Updating disturbance
    D_time(1:la)=u(2,k:k+la-1);

    
    H=2*(B'*Q*B+R);
    f=2*x'*(A'*Q*B)+2*D_time'*(Bd'*Q*B);        
     options =  optimoptions(@quadprog,'Display','off');
    
%     xlim=ones(system_size*la,1)*99999;
%     xlim(2:system_size:end)=0.5;
%     E=eye( la*system_size);
%     e=zeros(system_size);
%     e(2,2)=1;
%     E=e;
%     n=system_size;
%      for i=2:la
%         Z=zeros(n*(i-1),n);
%         E=[E ; e^(i)];
%      end
%     n=0.02;
%     %second dim E is 30 
%     E=E';
%     xlim=[1000 0.1 1000 1000]';
%     V=E*B;
%     w=xlim-E*Bd*D_time;

%   [xres1,fval,exitflag]=quadprog(H,f,V,w,[],[],[],[],[],options );
%      [xres1,fval,exitflag]=quadprog(H,f,[],[],[],[],[],[],[],options );
exitflag=1;
        xres1 = - inv(H)*f';
    xres2=xres1*0;
    xres2(1)=u(1,1)+xres1(1);
    for kk=2:length(xres1)
        xres2(kk)=xres2(kk-1)+xres1(kk);
    end
       xres= xres2;
    u(1,1:la)=xres';
    
    %Creates Q inputs (long vectors) for the simulation
    qu_used=ones(floor(sampling_time/dt)+4,1)*(u(1,1)+q);       %real controlled input   
    qd_used=ones(floor(sampling_time/dt)+4,1)*(u(2,k)+q);       %real disturbance
    if k==1
        [Time,Hm2,Hm,HinitH]=FiniteVoumeModel11bck(L,m,Bw,n,Sb,sampling_time,dt,qu_used,qd_used,y4(k),300,0,0);
       %                      FiniteVoumeModel11bck(L, z,Bw,n,s,SimTime,dt,qu,qd,h,NodeNum, UseInitialization, HinitGiven )
    else
        [Time,Hm2,Hm,HinitH]=FiniteVoumeModel11bck(L,m,Bw,n,Sb,sampling_time,dt,qu_used,qd_used,y4(k),300,1,Hm2(end,:));
    end
    
    %Saving variables
    y4(k+1)=Hm2(end,end);
    u_m(k)=u(2,k);
    u_m2(k)=u(1,1);
    
    Error(k+1)=abs(y4(k+1)-Y0);         %for analyses
    exitflagm(k)=exitflag;
end

%% analyses
y_ana=zeros(1,floor(simulation_time/sampling_time));
y_ana(1,:)=Y0; 
Error_steady_abs=abs(y4(end)-Y0);               %for analyses
Error_steady_rel=abs(y4(end)-Y0)/Y0;            %for analyses
Error_max_rel=max(Error)/Y0;                    %for analyses
Error_mean_rel=mean(Error)/Y0;                  %for analyses
Error_max=max(abs(Error));                    %for analyses
Error_mean=mean(abs(Error));                  %for analyses

Switch = 0;
SettleTime=inf;
%Threshold = 0.001;
Threshold = 0.015;
for k=length(Error):-1:1
    if abs(Error(k))> Threshold && k==length(Error)
        SettleTime = inf;
    else    
    if abs(Error(k)) > Threshold && Switch ==0
        SettleTime=k*sampling_time;
        Switch = 1;
    end
    end
end

% fprintf('Absolute steady state error = %f m\n', Error_steady_abs);
% fprintf('Relative steady state error = %f m\n', Error_steady_rel);
% fprintf('Relative maximum error      = %f m\n', Error_max_rel);
% fprintf('Relative mean error         = %f m\n', Error_mean_rel);

% % plots
% figure
% subplot(2,1,1)
% plot(y4,'--*');grid
% hold on
% plot(y_ana)
% title('Water level (downstream)')
% xlabel('time (h)')
% ylabel('water level (m)')
% 
% subplot(2,1,2)
% plot(u_m, '-*')
% hold on
% plot(u_m2, '-*r');grid
% % legend('Downstream (given) Q', 'Upstream (Controlled) Q','location','BestOutside')
% title('Discharges')
% xlabel('time (h)')
% ylabel('discharge (m3/s)')
