clear all; close all; 
addpath('../Functions')
addpath('ModelFunctions')
%% flow parameters
type=5;                 %type of channel [1/3/4/5]
trapezoidal=true;      %trapezoidal case? [true/false]
positive=true;         %positive disturbance? [true/false]


%% variables to tune
factor_sim_time=30;     %simulation length parameter
eMAVE=0.004;            %tuning factor; Maximum Allowed Value Estimate of the water level deviation
duMAVE=0.02;            %tuning factor; Maximum Allowed Value Estimate of the change in discharge
eMAVEMatrix = [0.02 0.015  0.010  0.009 0.008 0.007  0.006  0.005  0.004 0.003 0.002 0.001 ];
eMAVEMatrix = [0.03 0.25 0.02 0.015   0.010  0.005 ];
%eMAVEMatrix=0.03:-0.001:0.001;
%eMAVEMatrix=0.01:-0.001:0.001;
eMAVEMatrix=0.07:-0.005:0.03;
%eMAVEMatrix=0.015:-0.001:0.001;

%eMAVEMatrix=0.003:-0.0002:0.0002;
eMAVEMatrix=0.001:-0.0002:0.0001;
%eMAVEMatrix=[0.014];
%eMAVEMatrix=0.1:-0.01:0.01;
%duMAVE=1;
%eMAVEMatrix=0.5:-0.05:0.1;
%duMAVE=0;            %tuning factor; Maximum Allowed Value Estimate of the change in discharge
eMAVEMatrix=0.00006;
SettleTimeMatrix=zeros(length(eMAVEMatrix),4);
TypeMatrix=[1];

for TypeNum=1:length(TypeMatrix)
    type=TypeMatrix(TypeNum);                 %type of channel [1/3/4/5]
    [Y0,q,q_downstream,Sb,m] = Type(type,trapezoidal); 

for k = 1:length(eMAVEMatrix)
eMAVE=eMAVEMatrix(k);
%% constants 

n=0.02;                     %Manning factor [-]
Bw=3;                       %Bottom width [m]

h_real_initial=Y0;          %Initial downstream water height [m] 
L=7000;                     %Channel length [m]
kh=0;                       %Nothing
Mr_db=0;                    %Nothing - for IR model
Om_r=0;                     %Nothing - for IR model
UseTravelTime=0;            %Nothing - for IR model
NodeNumGiven=30;            %for Inertial model

% to switch from positive to negative step-disturbance
if positive==false
q_t=q;
q=q_downstream;
q_downstream=q_t;
end




%% gathering results from different internal models
%ID model
%[t_ID,tt_ID,y4_ID,y_ana_ID,u_m_ID,u_m2_ID,Error_steady_abs_ID,Error_steady_rel_ID,Error_max_rel_ID,Error_mean_rel_ID, Error_max, SettleTime]=Fun_ClosedLoopControl_ID_test(q,n,Bw,m,Sb,Y0,h_real_initial,L,kh,q_downstream,factor_sim_time,eMAVE,duMAVE);

% %IDZ model
 %[t_IDZ,tt_IDZ,y4_IDZ,y_ana_IDZ,u_m_IDZ,u_m2_IDZ,Error_steady_abs_IDZ,Error_steady_rel_IDZ,Error_max_rel_IDZ,Error_mean_rel_IDZ, Error_max, SettleTime]=Fun_ClosedLoopControl_IDZ_test(q,n,Bw,m,Sb,Y0,h_real_initial,L,kh,q_downstream,factor_sim_time,eMAVE,duMAVE);
% 
% %Inertial model
 [t_In,tt_In,y4_In,y_ana_In,u_m_In,u_m2_In,Error_steady_abs_In,Error_steady_rel_In,Error_max_rel_In,Error_mean_rel_In, Error_max, SettleTime]=Fun_ClosedLoopControl_Inertial_test(q,n,Bw,m,Sb,Y0,h_real_initial,L,kh,q_downstream,NodeNumGiven,factor_sim_time,eMAVE,duMAVE);
% 
% %Hayami model
% if (type ~=5) && (type~=4 && trapezoidal~=1)
  %   [t_H,tt_H,y4_H,y_ana_H,u_m_H,u_m2_H,Error_steady_abs_H,Error_steady_rel_H,Error_max_rel_H,Error_mean_rel_H, Error_max, SettleTime]=Fun_ClosedLoopControl_hayami_test(q,n,Bw,m,Sb,Y0,h_real_initial,L,kh,q_downstream,factor_sim_time,eMAVE,duMAVE);
% end
% 
% %Muskingum model
% [t_M,tt_M,y4_M,y_ana_M,u_m_M,u_m2_M,Error_steady_abs_M,Error_steady_rel_M,Error_max_rel_M,Error_mean_rel_M, Error_max, SettleTime]=Fun_ClosedLoopControl_Muskingum_test(q,n,Bw,m,Sb,Y0,h_real_initial,L,kh,q_downstream,factor_sim_time,eMAVE,duMAVE);
% 
% %IR model
% [t_IR,tt_IR,y4_IR,y_ana_IR,u_m_IR,u_m2_IR,Error_steady_abs_IR,Error_steady_rel_IR,Error_max_rel_IR,Error_mean_rel_IR, Error_max, SettleTime]=Fun_ClosedLoopControl_IR_test(q,n,Bw,m,Sb,Y0,h_real_initial,L,kh,q_downstream,Mr_db,Om_r,UseTravelTime,factor_sim_time,eMAVE,duMAVE);
% 
SettleTimeMatrix(k,TypeNum)=SettleTime/3600;
Error_maxMatrix(k)=Error_max;
end
end

%% visualizing resutls
figure
% subplot(2,1,1)
plot(y_ana_In,'r','Linewidth',2)
hold on
plot(y4_In,'k')

figure
plot(u_m2_In)

%,t_IDZ,y4_IDZ,'--k',t_In,y4_In,':k',t_IR,y4_IR,'-.k','Linewidth',2);grid 
%hold on
% plot(t_M,y4_M,'Color',[0.7 0.7 0.7],'Linewidth',2)
% if (type ~=5) && (type~=4 && trapezoidal~=1)
%     hold on
%     plot(t_H,y4_H,'-.k','Color',[0.7 0.7 0.7],'Linewidth',2)
% end
title('Water level (downstream)')
xlabel('time (h)')
ylabel('water level (m)')
if positive==true
    legend('Setpoint','ID', 'IDZ','Inertial','IR','Muskingum','Hayami','location','southeast')
else
    legend('Setpoint','ID', 'IDZ','Inertial','IR','Muskingum','Hayami','location','northwest')
end




% if Error_steady_abs_ID<0.01
%     test_ID=true;
% else
%     test_ID=false;
% end
% 
% if Error_steady_abs_IDZ<0.01
%     test_IDZ=true;
% else
%     test_IDZ=false;
% end
% 
% if Error_steady_abs_In<0.01
%     test_In=true;
% else
%     test_In=false;
% end
% 
% if (type ~=5) && (type~=4 && trapezoidal~=1)
%     if Error_steady_abs_H<0.01
%         test_H=true;
%     else
%         test_H=false;
%     end
% end
% 
% if Error_steady_abs_M<0.01
%     test_M=true;
% else
%     test_M=false;
% end
% 
% if Error_steady_abs_IR<0.01
%     test_IR=true;
% else
%     test_IR=false;
% end
% 
% if (type ~=5) && (type~=4 && trapezoidal~=1)
%     error_table = table([Error_steady_abs_ID;Error_steady_rel_ID;Error_mean_rel_ID;Error_max_rel_ID;test_ID],[Error_steady_abs_IDZ;Error_steady_rel_IDZ;Error_mean_rel_IDZ;Error_max_rel_IDZ;test_IDZ],[Error_steady_abs_In;Error_steady_rel_In;Error_mean_rel_In;Error_max_rel_In;test_In],[Error_steady_abs_H;Error_steady_rel_H;Error_mean_rel_H;Error_max_rel_H;test_H],[Error_steady_abs_M;Error_steady_rel_M;Error_mean_rel_M;Error_max_rel_M;test_M],[Error_steady_abs_IR;Error_steady_rel_IR;Error_mean_rel_IR;Error_max_rel_IR;test_IR],'VariableNames',{'ID','IDZ','Inertial','Hayami','Muskingum','IR'},'RowNames',{'Absolute steady state error [m]','Relative steady state error [m]','Relative mean error [m]','Relative max error [m]','Suitable [1=Yes, 0=No]'}) %
% else
%     error_table = table([Error_steady_abs_ID;Error_steady_rel_ID;Error_mean_rel_ID;Error_max_rel_ID;test_ID],[Error_steady_abs_IDZ;Error_steady_rel_IDZ;Error_mean_rel_IDZ;Error_max_rel_IDZ;test_IDZ],[Error_steady_abs_In;Error_steady_rel_In;Error_mean_rel_In;Error_max_rel_In;test_In],['-';'-';'-';'-';'0'],[Error_steady_abs_M;Error_steady_rel_M;Error_mean_rel_M;Error_max_rel_M;test_M],[Error_steady_abs_IR;Error_steady_rel_IR;Error_mean_rel_IR;Error_max_rel_IR;test_IR],'VariableNames',{'ID','IDZ','Inertial','Hayami','Muskingum','IR'},'RowNames',{'Absolute steady state error [m]','Relative steady state error [m]','Relative mean error [m]','Relative max error [m]','Suitable [1=Yes, 0=No]'}) %
% end
