function sf0_=sf0(y,q,n,b0,m)
a=b0*y+m*y^2;
p=b0+2*y*(1+m^2)^0.5;
r=a/p;
sf0_=(q^2*n^2)/(a^2*r^(4/3));