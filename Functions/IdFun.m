function [ae tau T]=IdFun(q,n,B,m,Sb,Y0,L,kh)

kh=0;       %? but isn't used
sb=Sb;      %[m/m]=[-]
Man=n;      %?  but isn't used
b0=B;       %with of canal 
h=Y0;       %waterlevel
yx=h;       %[m]

%Calculationg the divison points in case of other canals-------------------

g=9.81;     %[m/s^2]
y2=yx;      %[m]


% 
% % Part 3 ----Downstream part------------------------------------------------
% 
 a=b0*y2+m*y2^2;            %flow area canal [m^2]
% p=b0+2*y2*(1+m^2)^0.5;
% r=a/p;
 T=b0+2*y2*m;               %with of canal [m] + term with side slope m
 c=(g*a/T)^0.5;             %velocity of the wave [m/s]
 v=q/a;                     %flow velocity [m/s]
 
 tau=L/(c+v)/2+L/(c-v)/2;   %(same as K)[s]
 ae=L*T;                    %upper surface of the canal [m^2]
