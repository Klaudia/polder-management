function [num,den,sysidr,Mr_calc,Om_r_calc, num2, Om_r_calc_tr, DR]=ident_idr_new(b0 ,h , m ,q ,x,n,Mr_db,Om_r,UseTravelTime)

%This function caclulates the ir model from geometric properties
%Klauida Horvth 2012
%Modified 2017

%If it is not mentioned, the res.fr. used given in the paper, not the
%traveltime based
switch nargin
    case 8
        UseTravelTime=0;
end


%% Calculations related to geometry

g=9.81;
a=b0*h+m*h^2;
p=b0+2*h*(1+m^2)^0.5;
r=a/p;
v=q/a;
c=(9.81*h)^0.5;
b_=b0+2*h*m;
l=x;
travel_time=l/(c+v)+l/(c-v);
A=b_*x;

%Chezy coefficient
ch=1/n*r^(1/6);

%Calculated peak and frequency
m_calc=ch^2*r*h/(2*g*q*x);
Mr_calc= 20*log10(m_calc);

%Calculation of the resonance frequncy (Comment out the option you want)
%1) It can be just fitting a resonance model, and using : 2/travel_time*pi
%2) It can be the one used in the article : ((8*g*h)^0.5/x)

Om_r_calc=((8*g*h)^0.5/x);
Om_r_calc_tr=2/travel_time*pi;

%If the frequency and the peak is not given the calculated is used
 if Mr_db==0
     Mr_db=Mr_calc;
 end

 if Om_r==0
    Om_r=Om_r_calc;
 end
%If the travel time fr. is chosen, it is used
 if UseTravelTime==1
         Om_r=Om_r_calc_tr;
 end
 
%Calculation of the peak not in decibels
Mr=10^(Mr_db/20);

%Calculation of the damping
DR=1/(2*Om_r*A*Mr);

%Construction the transfer function for the upstream and downstream effects
num=[Om_r^2/A];
den=[1 2*DR*Om_r Om_r^2 0];
num2=-[2 2/A/Mr Om_r^2]/A; %The numerator for
num2=-[2 4*Om_r*DR Om_r^2]/A; %The numerator for

sysidr=tf([Om_r^2/A],[1 2*DR*Om_r Om_r^2 0]);

