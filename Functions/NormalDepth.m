function [yn]=NormalDepth(q,n,B,m,Sb,Y0)

%This is the implementation of the Integrator Delay model
%More information: 
%Schuurmans, Jan, et al. "Modeling of irrigation and drainage canals for controller design." 
%Journal of irrigation and drainage engineering 125.6 (1999): 338-344.

%Calculationg the divison points in case of other canals-------------------

g=9.81;

kh=0;       %initial height = 0
sb=Sb;      %bottom slope
Man=n;      %Manning coefficient??
b0=B;       %Bottom width
h=Y0;       %initial height??
yx=h;       %downstream height
g=9.81;

%Calculationg the divison points in case of other canals-------------------

% Calculating the normal depth

    %Normal depth
    
    y1=0;                                   %We suppose it is between 0 and 5 times the original depth
    y2=yx*5;        
    for k=1:25          
      y=(y1+y2)/2;                          %first guess (middle of the range)
      a=b0*y+m*y^2;                         %using average height to calculate area     
      p=b0+2*y*(1+m^2)^0.5;                 %wet perimeter calculated trapezoidal cross section
      r=a/p;                                %hydrolic radius
      dif=(q^2*n^2)/(a^2*r^(4/3))-sb;       %friction slope - bottom slope
      if dif<0                              % if bottom slope > friction slope, we are under normal depth
          y2=(y1+y2)/2;
      else                                  % if bottom slope < friction slope, we are over normal depth, lower range is moved to half
          y1=(y1+y2)/2;
      end
    end
    yn=y;                                   %Normal depth
