clear all
close all
filename_fig = 'Type5Rect';

open(strcat(filename_fig,'.fig')) %open your fig file, data is the name I gave to my file
D=get(gca,'Children'); %get the handle of the line object
XData=get(D,'XData'); %get the x data
YData=get(D,'YData'); %get the y data
%Data=[XData' YData']; %join the x and y data on one array nx2
%Data=[XData;YData]; %join the x and y data on one array 2xn
location = 'c:\Users\horvath\04_Research\04.01_Journal_papers\Modelling_paper\files\test_figure_making_new_matlab\';
filename = strcat(filename_fig,'.txt');
max_length=0;
for i=1:length(XData)
    if length(XData{i}) > max_length
        max_length = length(XData{i});
    end
end

all_data=ones(max_length,2*length(XData))*NaN;
for i=1:length(XData)
    for k=1:length(XData{i})
        all_data(k,i*2-1)=XData{i}(k);
        all_data(k,i*2) = YData{i}(k);
    end
end
[row_num, col_num]=size(all_data);

formatting_string = [];
formatting_header = [];

s = ' ';

for i=1:col_num
    formatting_string = strcat(formatting_string, '%24.6f', s);
    formatting_header = strcat(formatting_header, '%24s', s);
end
formatting_string = strcat(formatting_string, '\r\n');
%Check
f=figure(2);
my_legend={};
my_colors={'y', 'm', 'c', 'r', 'g', 'b','k','--y', '--m', '--c', '--r', '--g', '--b','--k'};
hold on
for i=1:length(XData)
    plot(all_data(:,i*2-1),all_data(:,i*2),my_colors{i});
    my_legend{i}=num2str(i);
end
legend(my_legend)
[fileID,b] = fopen(strcat(location, filename),'w+');

labels_array={'Hay', 'Mus', 'IR', 'In', 'IDZ', 'ID', 'Setpoint', 'IDZ', 'IDZ', 'IDZ', 'IDZ'};
for i=1:col_num/2
    fprintf(fileID,'%24s', 'Time(h)')
    fprintf(fileID,'%24s',strcat('Level_', labels_array{i},s,'(m)'))
end
fprintf(fileID,'\n')
    
fprintf(fileID,formatting_string,all_data');
fclose(fileID);
