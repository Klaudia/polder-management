clear all
close all
filename_fig = 'Type1Rect_bode';

open(strcat(filename_fig,'.fig')) %open your fig file, data is the name I gave to my file
for phase_of_mag=1:2
    close(figure(2))
     
    D=findobj(subplot(2,1,phase_of_mag), 'Type', 'line');
    XData=get(D,'XData'); %get the x data
    YData=get(D,'YData'); %get the y data

    location = 'c:\Users\horvath\04_Research\04.01_Journal_papers\Modelling_paper\files\test_figure_making_new_matlab\';
    if phase_of_mag==1
    filename = strcat(filename_fig,'_magnitude','.txt');
    else
            filename = strcat(filename_fig,'_phase','.txt');
    end
    max_length=0;
    for i=1:length(XData)
        if length(XData{i}) > max_length
           max_length = length(XData{i});
        end
    end

all_data=ones(max_length,2*length(XData))*NaN;
for i=1:length(XData)
    for k=1:length(XData{i})
        all_data(k,i*2-1)=XData{i}(k);
        all_data(k,i*2) = YData{i}(k);
    end
end
[row_num, col_num]=size(all_data);

formatting_string = [];
formatting_header = [];

s = ' ';

for i=1:col_num
    formatting_string = [formatting_string, '%24.6f', s];
    formatting_header = [formatting_header, '%24s', s];
end
formatting_string = strcat(formatting_string, '\n');
%Check
f=figure(2);
my_legend={};
my_colors={'y', 'm', 'c', 'r', 'g', 'b','k','--y', '--m', '--c', '--r', '--g', '--b','--k'};
hold on
for i=1:length(XData)
    plot(log10(all_data(:,i*2-1)),all_data(:,i*2),my_colors{i});
    my_legend{i}=num2str(i);
end
legend(my_legend)
[fileID,b] = fopen(strcat(location, filename),'w+');
% 1 Hay 2Mus  3IR   4In    5  IDZ  6 ID 7 SV
labels_array={'Hay', 'Mus', 'IR', 'In', 'IDZ', 'ID', 'SV', 'IDZ', 'IDZ', 'IDZ', 'IDZ'};
for i=1:col_num/2
    fprintf(fileID,'%24s', 'Frequency(rad/s)');
    if phase_of_mag==1
        fprintf(fileID,'%24s',strcat('Magnitude_', labels_array{i},s,'(-)'));
    else
         fprintf(fileID,'%24s',strcat('Phase_', labels_array{i},s,'(deg)'));
    end
end
fprintf(fileID,'\n')
all_data2=all_data(1:2,:);    
fprintf(fileID,formatting_string,all_data');
fclose(fileID);
end

