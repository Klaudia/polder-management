function [t,tt,y4,y_ana,u_m,u_m2,Error_steady_abs,Error_steady_rel,Error_max_rel,Error_mean_rel]=Fun_ClosedLoopControl_IDZ(q,n,Bw,m,Sb,Y0,h_real_initial,L,kh,q_downstream,factor_sim_time,eMAVE,duMAVE)

[ae tau T]=IdFun(q,n,Bw,m,Sb,Y0,L,kh);

%% Simulation variables
sampling_time=900;          %sampling time [s]
dt=sampling_time/10;        %discretization time of the SV model [s] -- Should be an integer
simulation_time=tau*factor_sim_time;     %Simulation time [s]

%Uncomment this if you would like to see the profile
% [ WaterProfile, distance] = SteadyState(L, m,Bw,n,Sb,q,Y0,100, 0);
% plot(WaterProfile)

%% Create a system by hand

%internal IDZ model
[p11_inf_hat,p12_inf_hat,p21_inf_hat,p22_inf_hat,au_hat,ad_hat,tu_hat,td_hat,yn,x2]=IdzFun(q,n,Bw,m,Sb,Y0,L);

int=floor(td_hat/sampling_time);        %integrator [t_d/dt]
t_df=td_hat-sampling_time*int;          

%constructing groups
g1=(ad_hat*p21_inf_hat-t_df+sampling_time)/ad_hat;      
g2=(ad_hat*p21_inf_hat-t_df)/ad_hat;
g3=(ad_hat*p22_inf_hat+sampling_time)/ad_hat;           
g4=(ad_hat*p22_inf_hat)/ad_hat;

system_size = 3+int;                 
a=zeros(system_size,system_size);
if int==0
    a(1,1)=1;
    a(1,2)=-g2;
    a(1,3)=g4;
else
    a(1,1)=1;
    a(1,end-2)=g1;
    a(1,end-1)=-g2;
    a(1,end)=g4;
    for k=1:int
        a(k+2,k+1)=1;
    end
end

b=zeros (system_size, 2);
if int==0
    b(1,1)=g1;
    b(1,2)=-g3;
    b(2,1)=1;
    b(3,2)=1;
else
    b(1,2)=-g3;
    b(2,1)=1;
    b(end,2)=1;
end

c=zeros (1,system_size);
c(1)=1;

%% Simulation
la=10;          %prediction horizon

simulation_time_vector=0:sampling_time:simulation_time;
t=0:sampling_time/3600:simulation_time/3600;                       %for adding time-scale to the plots
tt=0:sampling_time/3600:(simulation_time-1*sampling_time)/3600;    %for adding time-scale to the plots

%input discharges
u=zeros(2,length(simulation_time_vector)+la);
u(1,1)=0;
u(2,14)=q_downstream-q;
u(2,15)=q_downstream-q;
u(2,16)=q_downstream-q;

x0=zeros(system_size,1);
x0(1,1)=h_real_initial-Y0;

%% Creating the big matrices
[A,B,C,Bd,Q,R]=MatrixContructionFunctionNew(a,b(:,1),c,b(:,2),la,eMAVE,duMAVE);

%% creating uncontrolled behaviour
x=x0;
x(1,1)=h_real_initial-Y0;
y6_real(1)=h_real_initial;

for k=1:length(u)-1
    x=a*x+b*u(:,k);
    y=c*x;
    y6(k+1)=y;
    y6_real(k+1)=y6(k+1)+Y0;
    ttt(k+1)=k*sampling_time/3600;
end

% figure
% plot(ttt,y6_real);grid
% title('Water level (downstream) after step in downstream discharge')
% xlabel('time (h)')
% ylabel('water level (m)')

%% Now creating a control

x=x0;
y4(1)=h_real_initial;
D_time=zeros(la,1);
u_int1=u;
u_int2=u;
u_int3=u;
u_int4=u;

for k=1:floor(simulation_time/sampling_time)
    
    xtest=a*x+b*[u(1,1);D_time(1)];      %updating discharge in x
    x=xtest;
    x(1,1)=y4(k)-Y0;        %updating the water level difference every time step

    %Updating disturbance
    D_time(1:la)=u(2,k:k+la-1);
  
    H=2*(B'*Q*B+R);
    f=2*x'*(A'*Q*B)+2*D_time'*(Bd'*Q*B);        

    [xres,fval,exitflag]=quadprog(H,f);

    u(1,1:la)=xres';
    
    %Creates Q inputs (long vectors) for the simulation
    qu_used=ones(floor(sampling_time/dt)+4,1)*(u(1,1)+q);       %real controlled input   
    qd_used=ones(floor(sampling_time/dt)+4,1)*(u(2,k)+q);       %real disturbance
    if k==1
        [Time,Hm2,Hm,HinitH]=FiniteVoumeModel11bck(L,m,Bw,n,Sb,sampling_time,dt,qu_used,qd_used,y4(k),300,0,0);
    else
        [Time,Hm2,Hm,HinitH]=FiniteVoumeModel11bck(L,m,Bw,n,Sb,sampling_time,dt,qu_used,qd_used,y4(k),300,1,Hm2(end,:));
    end
    
    %Saving variables
    y4(k+1)=Hm2(end,end);
    u_m(k)=u(2,k);
    u_m2(k)=u(1,1);
    
    Error(k+1)=abs(y4(k+1)-Y0);         %for analyses
end
y2_real(1)=h_real_initial;

%% analyses
y_ana=zeros(1,floor(simulation_time/sampling_time));
y_ana(1,:)=Y0; %*(1-0.005*q_downstream);
Error_steady_abs=abs(y4(end)-Y0);               %for analyses
Error_steady_rel=abs(y4(end)-Y0)/Y0;            %for analyses
Error_max_rel=max(Error)/Y0;                    %for analyses
Error_mean_rel=mean(Error)/Y0;                  %for analyses

% fprintf('Absolute steady state error = %f m\n', Error_steady_abs);
% fprintf('Relative steady state error = %f m\n', Error_steady_rel);
% fprintf('Relative maximum error      = %f m\n', Error_max_rel);
% fprintf('Relative mean error         = %f m\n', Error_mean_rel);

%% plots
% figure
% subplot(2,1,1)
% plot(t,y4,'--*');grid
% hold on
% plot(tt,y_ana)
% title('Water level (downstream)')
% xlabel('time (h)')
% ylabel('water level (m)')
% 
% subplot(2,1,2)
% plot(tt,u_m, '-*')
% hold on
% plot(tt,u_m2, '-*r');grid
% % legend('Downstream (given) Q', 'Upstream (Controlled) Q','location','BestOutside')
% title('Discharges')
% xlabel('time (h)')
% ylabel('discharge (m3/s)')