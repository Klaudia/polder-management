%Example
clear; close all; clc
addpath('../Functions')

q=2;                        %Steady initial inflow [m^3/s]
n=0.02;                     %Manning factor [-]
Bw=3;                       %Bottom width [m]
m=0;                        %Side slope [m/m]
Sb=0.0003;                  %Bottom slope [m/m]
Y0=1;                       %Desired downstream water level [m]
h_real_initial=Y0;          %Initial downstream water height [m] 
L=7000;                     %Channel length [m]
kh=0;                       %Nothing
q_downstream=2.5;           %Downstream discharge [m^3/s]

[ae tau]=IdFun(q,n,Bw,m,Sb,Y0,L,kh);


%% Simulation variables
sampling_time=900;          %sampling time [s]
dt=sampling_time/10;        %discretization time of the SV model [s] -- Should be an integer
simulation_time=tau*30;     %simulation time [s]

%Uncomment this if you would like to see the profile
% [ WaterProfile, distance] = SteadyState(L, m,Bw,n,Sb,q,Y0,100, 0);
% plot(WaterProfile)
%% Create a system by hand

%Internal Hayami model
[aa,bb,cc,dd,ee,r,cl,yn,ccnoni,ddnoni,eenoni,G,K1,tau,ae]=FunHayami(q,n,Bw,m,Sb,Y0,L,kh,dt);

int=floor(tau/sampling_time);       %integrator [t_d/dt]
t_f=tau-sampling_time*int;      

if cl<9/4       %first order with(out) delay
    n2=(K1*exp(-1*(sampling_time-t_f)/K1)+sampling_time-t_f-K1)/ae;
    n1=(-2*K1*exp(-1*(sampling_time-t_f)/K1)+t_f+(-sampling_time+t_f+K1)*exp(-sampling_time/K1)+K1)/ae;
    n0=(K1*exp(-1*(sampling_time-t_f)/K1)+(-t_f-K1)*exp(-sampling_time/K1))/ae;
    d1=(-1-exp(-sampling_time/K1));
    d0=exp(-sampling_time/K1);

else            %second order with delay
    K2=K1(2);
    K1=K1(1);
    a1=exp(-sampling_time/K1);
    a2=exp(-sampling_time/K2);
    a3=(K1^2)/(K1-K2)*exp(-1*(sampling_time-t_f)/K1);
    a4=(K2^2)/(K1-K2)*exp(-1*(sampling_time-t_f)/K2);

    n3=(a3-a4+(sampling_time-t_f)-(K1+K2))/ae;
    n2=(-2*a3+2*a4-(sampling_time-t_f)*(a1+a2)+t_f-(K1+K2)*(-1*(a1+a2+1))-a2*a3+a1*a4)/ae;
    n1=(a3*(1+2*a2)-a4*(1+2*a1)+(sampling_time-t_f)*a1*a2-t_f*(a1+a2)-(K1+K2)*(a1+a2+a1*a2))/ae;
    n0=(-a2*a3+a1*a4+t_f*a1*a2+(K1+K2)*a1*a2)/ae;
    d2=(-a1-a2-1);
    d1=(a1+a2+a1*a2);
    d0=(-a1*a2);
end

%Construction of matrices
if cl<9/4       %first order with(out) delay
    system_size=8+int;                 %system size depends on delay
    a=zeros(system_size,system_size);
    if int==0
        a(1,1)=1-d1;
        a(1,2)=d1-d0;
        a(1,3)=d0;
        a(1,4)=n1-n2;
        a(1,5)=n0-n1;
        a(1,6)=-n0;
        a(1,7)=-sampling_time/(ae)*d1;
        a(1,8)=-sampling_time/(ae)*d0;
        a(2,1)=1;
        a(3,2)=1;
        a(5,4)=1;
        a(6,5)=1;
        a(8,7)=1;
    else
        a(1,1)=1-d1;
        a(1,2)=d1-d0;
        a(1,3)=d0;
        a(1,end-5)=n2;
        a(1,end-4)=n1-n2;
        a(1,end-3)=n0-n1;
        a(1,end-2)=-n0;
        a(1,end-1)=-sampling_time/(ae)*d1;
        a(1,end)=-sampling_time/(ae)*d0;
        a(2,1)=1;
        a(3,2)=1;
        a(end,end-1)=1;
        for k=1:2+int
            a(k+4,k+3)=1;
        end
    end

    b=zeros(system_size,2);
    if int==0
        b(1,1)=n2;
        b(1,2)=-sampling_time/ae;
        b(4,1)=1;
        b(7,2)=1;
    else
        b(1,2)=-sampling_time/ae;
        b(4,1)=1;
        b(end-1,2)=1;
    end
    
    c=zeros(1,system_size);
    c(1)=1;
    
else            %second order with delay
    system_size=11+int;                 %system size is dependend on delay
    a=zeros(system_size,system_size);
    if int==0
        a(1,1)=1-d2;
        a(1,2)=d2-d1;
        a(1,3)=d1-d0;
        a(1,4)=d0;
        a(1,5)=n2-n3;
        a(1,6)=n1-n2;
        a(1,7)=n0-n1;
        a(1,8)=-n0;
        a(1,9)=-sampling_time/(ae)*d2;
        a(1,10)=-sampling_time/(ae)*d1;
        a(1,11)=-sampling_time/(ae)*d0;
        a(2,1)=1;
        a(3,2)=1;
        a(4,3)=1;
        a(6,5)=1;
        a(7,6)=1;
        a(8,7)=1;
        a(10,9)=1;
        a(11,10)=1;
    else
        a(1,1)=1-d2;
        a(1,2)=d2-d1;
        a(1,3)=d1-d0;
        a(1,4)=d0;
        a(1,end-7)=n3;
        a(1,end-6)=n2-n3;
        a(1,end-5)=n1-n2;
        a(1,end-4)=n0-n1;
        a(1,end-3)=-n0;
        a(1,end-2)=-sampling_time/(ae)*d2;
        a(1,end-1)=-sampling_time/(ae)*d1;
        a(1,end)=-sampling_time/(ae)*d0;
        a(2,1)=1;
        a(3,2)=1;
        a(4,3)=1;
        a(end-1,end-2)=1;
        a(end,end-1)=1;
        for k=1:int+3
            a(k+5,k+4)=1;
        end
    end

    b=zeros(system_size,2);
    if int==0
        b(1,1)=n3;
        b(1,2)=-sampling_time/ae;
        b(5,1)=1;
        b(9,2)=1;
    else
        b(1,2)=-sampling_time/ae;
        b(5,1)=1;
        b(end-2,2)=1;
    end
    
    c=zeros(1,system_size);
    c(1)=1;
end



%% Simulation
la=10;      %Prediction horizon

simulation_time_vector= 0:sampling_time:simulation_time;
t=0:sampling_time/3600:simulation_time/3600;                        %for adding time-scale to the plots
tt=0:sampling_time/3600:(simulation_time-1*sampling_time)/3600;     %for adding time-scale to the plots

%Input discharges
u=zeros(2,length(simulation_time_vector)+la);
u(1,1)=0;
u(2,14)=q_downstream-q;
u(2,15)=q_downstream-q;
u(2,16)=q_downstream-q;

x0=zeros(system_size,1);
x0(1,1)=h_real_initial-Y0;

%% Creating the big matrices
[A,B,C,Bd,Q,R]= MatrixContructionFunctionNew(a,b(:,1),c,b(:,2),la,0.015,0.03);


%% creating uncontrolled behaviour
x=x0;
x(1,1)=h_real_initial-Y0;
y6_real(1)=h_real_initial;

for k=1:length(u)-1
    x=a*x+b*u(:,k);
    y=c*x;
    y6(k+1)=y;
    y6_real(k+1)=y6(k+1)+Y0;
    ttt(k+1)=k*sampling_time/3600;
end

%% SV-model
UseInitialization=0;
HinitGiven=0;
NodeNum=300;
u_SV=ones(2,(length(simulation_time_vector)+la)*10);
u_SV(1,14*10:16*10+9)=q_downstream;

[Time,Hm2_SV,Hm,HinitH]=FiniteVoumeModel11bck(L,m,Bw,n,Sb,simulation_time+(la-1)*sampling_time,dt,u_SV(1,:),u_SV(2,:),Y0,300,0,0);
tttt=0:dt/3600:(simulation_time+(la-1)*sampling_time)/3600;
tttt(end)=12.25;
 
y6_real_step=y6_real(14:end);
ttt_step=ttt(14:end)-3.25;
Hm2_SV_step=Hm2_SV(140:end,:);
tttt_step=tttt(140:end)-3.475;
tttt_step(end)=9;
 

%% plot step response
figure
plot(ttt_step,y6_real_step,tttt_step,Hm2_SV_step(:,end),'-k','linewidth',2);grid
title('Hayami model','FontSize',12,'FontWeight','bold')
xlabel('time (h)','FontSize',12,'FontWeight','bold')
ylabel('water level (m)','FontSize',12,'FontWeight','bold')
legend('Hayami model','Saint-Venant','location','southeast')

%% Now creating a control

x=x0;
y4(1)=h_real_initial;
D_time=zeros(la,1);

for k=1:floor(simulation_time/sampling_time)
   
    xtest=a*x+b*[u(1,1);D_time(1)];           %updating discharge in x
    x=xtest;
    x(1,1)=y4(k)-Y0;        %updating the water level difference every time step
 
    D_time(1:la) = u(2,k:k+la-1);

    H=2*(B'*Q*B+R);
    f=2*x'*(A'*Q*B)+2*D_time'*(Bd'*Q*B);        

    [xres,fval,exitflag]=quadprog(H,f);
    
    u(1,1:la)=xres';
   
    %Creates Q inputs (long vectors) for the simulation
    qu_used=ones(floor(sampling_time/dt)+4,1)*(u(1,1)+q);       %real controlled input   
    qd_used=ones(floor(sampling_time/dt)+4,1)*(u(2,k)+q);       %real disturbance
    if k==1
        [Time,Hm2,Hm,HinitH]=FiniteVoumeModel11bck(L,m,Bw,n,Sb,sampling_time,dt,qu_used,qd_used,y4(k),300,0,0);
    else
        [Time,Hm2,Hm,HinitH]=FiniteVoumeModel11bck(L,m,Bw,n,Sb,sampling_time,dt,qu_used,qd_used,y4(k),300,1,Hm2(end,:));
    end
    
    %Saving variables
    y4(k+1)=Hm2(end,end);
    u_m(k)=u(2,k);
    u_m2(k)=u(1,1);
    
    Error(k+1)=abs(y4(k+1)-Y0);         %for analyses
end
y2_real(1)=h_real_initial;

%% analyses
y_ana=zeros(1,floor(simulation_time/sampling_time));
y_ana(1,:)=Y0; %*(1-0.005*q_downstream);
Error_steady=abs(y4(end)-Y0);            %for analyses
Error_max=max(Error);                    %for analyses
Error_mean=mean(Error);                  %for analyses

fprintf('Steady state error = %f m\n', Error_steady);
fprintf('Maximum error      = %f m\n', Error_max);
fprintf('Mean error         = %f m\n', Error_mean);
fprintf('int                = %d \n', int);
fprintf('cl                = %d \n', cl);

%%
figure
subplot(2,1,1)
plot(t,y4,'--*');grid
hold on
plot(tt,y_ana)
title('Water level (downstream)')
xlabel('time (h)')
ylabel('water level (m)')

subplot(2,1,2)
plot(tt,u_m, '-*')
hold on
plot(tt,u_m2, '-*r');grid
% legend('Downstream (given) Q', 'Upstream (Controlled) Q','location','BestOutside')
title('Discharges')
xlabel('time (h)')
ylabel('discharge (m3/s)')