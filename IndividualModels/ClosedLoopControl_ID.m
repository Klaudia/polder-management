%Example
clear; close all; clc
addpath('../Functions')

q=1;                        %Steady initial inflow [m^3/s]
n=0.02;                     %Manning factor [-]
Bw=3;                       %Bottom width [m]
m=0;                        %Side slope [m/m]
Sb=0.0003;                  %Bottom slope [m/m]
Y0=3.5;                       %Desired downstream water level [m]
h_real_initial=Y0;          %Initial downstream water height [m] 
L=7000;                     %Channel length [m]
kh=0;                       %Nothing
q_downstream=1.5;           %Downstream discharge [m^3/s]

[ae, tau, T]=IdFun(q,n,Bw,m,Sb,Y0,L,kh);

%% Simulation variables
sampling_time=900;          %sampling time [s]
dt=sampling_time/10;        %discretization time of the SV model [s] -- Should be an integer
simulation_time=tau*30;     %simulation time [s]

%Uncomment this if you would like to see the profile
% [ WaterProfile, distance] = SteadyState(L, m,Bw,n,Sb,q,Y0,100, 0);
% plot(WaterProfile)
%% Create a system by hand

system_size=floor(tau/sampling_time)+1;
a=zeros(system_size,system_size);
a(1,1)=1;
a(1,end)=sampling_time/ae;
for k=3:system_size
    a(k,k-1)=1;
end

b=zeros(system_size,2);
b(2,1)=1;
b(1,2)=-sampling_time/ae;

c=zeros(1,system_size);
c(1)=1;

%% Simulation
la=10;        %prediction horizon

simulation_time_vector=0:sampling_time:simulation_time;            
t=0:sampling_time/3600:simulation_time/3600;                       %for adding time-scale to the plots
tt=0:sampling_time/3600:(simulation_time-1*sampling_time)/3600;    %for adding time-scale to the plots

%discharge input
u=zeros(2,length(simulation_time_vector)+la);
u(1,1)=0;
u(1,14)=q_downstream-q;
u(1,15)=q_downstream-q;
u(1,16)=q_downstream-q;

x0=zeros(system_size,1);
x0(1,1)=h_real_initial-Y0;

%% Creating the big matrices
[A,B,C,Bd,Q,R]=MatrixContructionFunctionNew(a,b(:,1),c,b(:,2),la,0.008,0.03);

%% creating uncontrolled behaviour
x = x0;
x(1,1)=h_real_initial-Y0;
y6_real=h_real_initial;

for k=1:length(u)-1
    x=a*x+b*u(:,k);
    y=c*x;
    y6(k+1)=y;
    y6_real(k+1)=y6(k+1)+Y0;
    ttt(k+1)=k*sampling_time/3600;
end

%% SV-model
UseInitialization=0;
HinitGiven=0;
NodeNum=300;
u_SV=ones(2,(length(simulation_time_vector)+la)*10);
u_SV(1,14*10:16*10+9)=q_downstream;

[Time,Hm2_SV,Hm,HinitH]=FiniteVoumeModel11bck(L,m,Bw,n,Sb,simulation_time+(la-1)*sampling_time,dt,u_SV(1,:),u_SV(2,:),Y0,300,0,0);
tttt=0:dt/3600:(simulation_time+(la-1)*sampling_time)/3600;
tttt(end)=12.25;
 
y6_real_step=y6_real(14:end);
ttt_step=ttt(14:end)-3.25;
Hm2_SV_step=Hm2_SV(140:end,:);
tttt_step=tttt(140:end)-3.475;
tttt_step(end)=9;
 

%% plot step response
figure
plot(ttt_step,y6_real_step,tttt_step,Hm2_SV_step(:,end),'-k','linewidth',2);grid
title('Integrator Delay model','FontSize',12,'FontWeight','bold')
xlabel('time (h)','FontSize',12,'FontWeight','bold')
ylabel('water level (m)','FontSize',12,'FontWeight','bold')
legend('ID model','Saint-Venant','location','southeast')

%% Now creating a control

x=x0;
y4(1)=h_real_initial;
D_time=zeros(la,1);

for k=1:floor(simulation_time/sampling_time)
    
    xtest=a*x+b*[u(1,1);D_time(1)];     %updating discharge in x
    x=xtest;
    x(1,1)=y4(k)-Y0;                    %updating the water level difference every time step
    
    %Updating disturbance
    D_time(1:la)=u(2,k:k+la-1);

    H=2*(B'*Q*B+R);
    f=2*x'*(A'*Q*B)+2*D_time'*(Bd'*Q*B);        

    [xres,fval,exitflag]=quadprog(H,f);
    
    u(1,1:la)=xres';
    
    %Creates Q inputs (long vectors) for the simulation
    qu_used=ones(floor(sampling_time/dt)+4,1)*(u(1,1)+q);       %real controlled input   
    qd_used=ones(floor(sampling_time/dt)+4,1)*(u(2,k)+q);       %real disturbance
    if k==1
        [Time,Hm2,Hm,HinitH]=FiniteVoumeModel11bck(L,m,Bw,n,Sb,sampling_time,dt,qu_used,qd_used,y4(k),300,0,0);
    else
        [Time,Hm2,Hm,HinitH]=FiniteVoumeModel11bck(L,m,Bw,n,Sb,sampling_time,dt,qu_used,qd_used,y4(k),300,1,Hm2(end,:));
    end
    
    %Saving variables
    y4(k+1)=Hm2(end,end);
    u_m(k)=u(2,k);
    u_m2(k)=u(1,1);
    
    Error(k+1)=abs(y4(k+1)-Y0);         %for analyses
end

%% analyses
y_ana=zeros(1,floor(simulation_time/sampling_time));
y_ana(1,:)=Y0; 
Error_steady_abs=abs(y4(end)-Y0);               %for analyses
Error_steady_rel=abs(y4(end)-Y0)/Y0;            %for analyses
Error_max_rel=max(Error)/Y0;                    %for analyses
Error_mean_rel=mean(Error)/Y0;                  %for analyses



fprintf('Absolute steady state error = %f m\n', Error_steady_abs);
fprintf('Relative steady state error = %f m\n', Error_steady_rel);
fprintf('Relative maximum error      = %f m\n', Error_max_rel);
fprintf('Relative mean error         = %f m\n', Error_mean_rel);

%% plots
figure
subplot(2,1,1)
plot(tt,y_ana)
hold on
plot(t,y4,'-*');grid  %,'--*'
title('Water level (downstream)')
xlabel('time (h)')
ylabel('water level (m)')

subplot(2,1,2)
plot(tt,u_m,'-*')
hold on
plot(tt,u_m2, '-*r');grid
% legend('Downstream (given) Q', 'Upstream (Controlled) Q','location','BestOutside')
title('Discharges')
xlabel('time (h)')
ylabel('discharge (m3/s)')